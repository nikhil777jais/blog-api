from django.contrib import admin
from django.contrib.admin.options import ModelAdmin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.db.models.base import Model
from .forms import UserChangeForm, UserCreationForm
from . models import Post
User = get_user_model()

@admin.register(User)
class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('id','email', 'is_superuser',)
    list_filter = ('is_superuser', 'is_staff', 'is_active')
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('name', 'phone', 'gender', 'date_of_birth',)}),
        ('Permissions', {'fields': ('is_active','is_staff', 'is_superuser',),}),
        ('Group Permissions', {
            'fields': ('groups', 'user_permissions',)
        }),
        ('Important dates', {'fields': ('last_login','date_joined'),}),
        
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email','name','phone', 'gender','password1', 'password2',),
        }),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ('groups', 'user_permissions',) #for horizontal view of permissions and group
    
@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    pass