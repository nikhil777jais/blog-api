from functools import partial
from django.http import response
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Post
from django.contrib.auth import get_user_model

User = get_user_model()
from .serializers import UserSignUpSerializer, PostSerializer
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import DjangoModelPermissions
from rest_framework_simplejwt.tokens import RefreshToken

# Create your views here.
class UserSignUpAPI(APIView):
  def get(self, request, id = None, format=None): 
    if id != None:
      user_obj = User.objects.get(pk=id)
      serializer = UserSignUpSerializer(user_obj)
      return Response({'payload': serializer.data, 'msg':'Data Retrived'}, status = status.HTTP_200_OK)
    user_obj = User.objects.all()
    serializer = UserSignUpSerializer(user_obj, many=True)
    return Response({'payload': serializer.data,  'msg':'Data Retrived'}, status = status.HTTP_200_OK)
  
  def post(self, request, format = None):
    serializer = UserSignUpSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      user = User.objects.get(email= serializer.data['email']) #get newly generated user
      refresh = RefreshToken.for_user(user) #generate a token for user just while signup
      return Response({'payload': serializer.data, 'refresh': str(refresh), 'access': str(refresh.access_token), 'msg':'Data Saved'}, status = status.HTTP_200_OK)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

#BlogFolALL API view
class BlogForAllAPI(APIView):
  queryset = User.objects.none()
  def get(self, request, id=None, format=None):
    if id != None:
      post_obj = Post.objects.get(pk=id) 
      serializer = PostSerializer(post_obj)
      return Response({'payload': serializer.data, 'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)
    post_obj = Post.objects.all()
    serializer = PostSerializer(post_obj, many=True)
    return Response({'payload': serializer.data, 'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)

class BlogAPI(APIView):
  authentication_classes = [JWTAuthentication]
  permission_classes = [DjangoModelPermissions]    
  queryset = User.objects.none()
  def get(self, request, id = None, format=None):
    if id != None:
      post_obj = Post.objects.get(pk=id) 
      serializer = PostSerializer(post_obj)
      return Response({'payload': serializer.data, 'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)
    post_user = request.user.blogs
    post_liked = request.user.liked_post
    post_obj = Post.objects.all()
    serializer_user = PostSerializer(post_user, many=True)
    serializer_liked = PostSerializer(post_liked, many=True)
    serializer_all = PostSerializer(post_obj, many=True)
    return Response({'payload': {'myblogs':serializer_user.data,'liked blogs':serializer_liked.data,'allblogs': serializer_all.data}, 'msg':'Data Retrived!!'}, status = status.HTTP_200_OK)

  def post(self, request, format = None):
    request.data['author'] = request.user.id
    print(request.data)
    serializer = PostSerializer(data=request.data) 
    if serializer.is_valid():
      serializer.save()
      return Response({'payload': serializer.data , 'msg':'Data Saved!!',}, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)  

  def patch(self, request, id = None, format = None):
    if request.user.blogs.filter(pk=id):
      post_obj = Post.objects.get(pk=id)
      serializer = PostSerializer(post_obj, data = request.data, partial=True)
      if serializer.is_valid():
        serializer.save()
        return Response({'payload':serializer.data , 'msg':'data updated partialy',}, status = status.HTTP_201_CREATED)
      return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)  
    return Response({'msg':'not your blog'}, status = status.HTTP_401_UNAUTHORIZED)
  
  def put(self, request, id = None, format = None):
    if request.user.blogs.filter(pk=id):
      post_obj = Post.objects.get(pk=id)
      serializer = PostSerializer(post_obj, data = request.data)
      if serializer.is_valid():
        serializer.save()
        return Response({'payload':serializer.data , 'msg':'data updated completely',}, status = status.HTTP_201_CREATED)
      return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)  
    return Response({'msg':'not your blog'}, status = status.HTTP_401_UNAUTHORIZED)

    
  def delete(self, request, id=None, format = None):
    if request.user.blogs.filter(pk=id):
      post_obj = User.objects.get(pk=id)
      post_obj.delete()
      return Response({'msg':'data deleted'}, status = status.HTTP_200_OK) 
    return Response({'msg':'not your blog'}, status = status.HTTP_401_UNAUTHORIZED)  
      
class LikeAPI(APIView):
  authentication_classes = [JWTAuthentication]
  permission_classes = [DjangoModelPermissions]
  queryset = User.objects.none()
  def post(self, request, id = None, format = None):
    post_obj = Post.objects.get(pk = id)
    post_obj.like.add(request.user.id)
    return Response({'msg':'You liked that post'}, status = status.HTTP_401_UNAUTHORIZED)  
    
