from rest_framework import serializers
from . models import Post
from django.contrib.auth import get_user_model
User = get_user_model()

#user serialiser
class UserSignUpSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ['email','password','name', 'phone', 'gender', 'date_of_birth','date_joined', 'last_login']  
    extra_kwargs = {
      'email': {'required': True},
      'passwors': {'required': True},
      'name': {'required': True},
      'phone': {'required': True},
      'date_joined': {'read_only': True},
      'last_login': {'read_only': True},
    }
  def create(self, validated_data):
    user = User.objects.create(email=validated_data['email'])
    user.set_password(validated_data['password'])
    user.name = validated_data['name']
    user.phone = validated_data['phone']
    user.gender = validated_data['gender']
    user.groups.add(1)
    user.date_of_birth = validated_data['date_of_birth']
    user.save()
    # if validated_data['role'] == 'student':
    #   for course in courses:
    #     c_obj = Course.objects.get(course_name=course['course_name'])
    #     user.courses.add(c_obj)
    return user 

  def update(self, instance, validated_data):
    instance.email = validated_data.get('email', instance.email)
    instance.set_password(validated_data.get('password', instance.password))
    instance.name = validated_data.get('name', instance.name)
    instance.phone = validated_data.get('phone', instance.phone)
    instance.gender = validated_data.get('gender', instance.gender)
    instance.date_of_birth = validated_data.get('date_of_birth', instance.date_of_birth)
    instance.save()
    # for course in courses:
    #   c_obj = Course.objects.get(course_name=course['course_name'])
    #   instance.courses.add(c_obj)
    return instance  

#Subject serialiser
class PostSerializer(serializers.ModelSerializer):
  class Meta:
    model = Post
    fields = ['id','title','description', 'likes','author']
    extra_kwargs = {
      'id': {'read_only': True, 'required': False},
      'title': {'required': True},
      'description': {'required': True},
      'author': {'required': False},
      }