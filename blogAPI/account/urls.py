from django.contrib import admin
from django.urls import path
from . import views
from rest_framework_simplejwt import views as jwt_views
urlpatterns = [
    path('u/', views.UserSignUpAPI.as_view(), name='u'),
    path('blogall/', views.BlogForAllAPI.as_view(), name='blogall'),
    path('blogall/<int:id>/', views.BlogForAllAPI.as_view(), name='blogall_id'),
    path('blog/', views.BlogAPI.as_view(), name='blog'),
    path('blog/<int:id>/', views.BlogAPI.as_view(), name='blog_id'),
    path('like/<int:id>/', views.LikeAPI.as_view(), name='like_id'),
    path('gettoken/', jwt_views.TokenObtainPairView.as_view(), name='gettoken'),
    path('refresh/', jwt_views.TokenRefreshView.as_view(), name='refresh'),
]
